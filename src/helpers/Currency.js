const getCurrency = (curr, precision, roundUp) => {
  let newCurr = 0
  if(roundUp) {
    newCurr = Math.ceil(curr)
  } else {
    newCurr = Math.round(curr * Math.pow(10, precision)) / Math.pow(10, precision)
  }
  return new Intl.NumberFormat().format(newCurr)
}

export default getCurrency
