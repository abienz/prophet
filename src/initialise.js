import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App/App';

let mountNode = document.getElementById('app');
ReactDOM.render((
  <App />
),mountNode);
