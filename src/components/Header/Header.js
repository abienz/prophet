import React from 'react'

const Header = (props) => {
  return(
    <header className="header-component">
      <h1>{props.heading}</h1>
    </header>
  )
}

export default Header
