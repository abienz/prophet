import React from 'react'

const TimelineCard = (props) => {
  const className = (props.type) ? props.type + ' timelineCard-component' : 'timelineCard-component'

  return(
    <div className={className}>
      <span>Read 8 Aug 2018</span>
      <h2>Daily roundup</h2>
      <p>See yesterday's money in and money out, and what's coming up tomorrow</p>
    </div>
  )
}

export default TimelineCard
