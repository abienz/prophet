import React from 'react'
import Moment from 'moment'
import getCurrency from '../../helpers/Currency'

const SpendingList = (props) => {
  const toggleClass = props.showAll
  const title = (props.isRegular) ? 'Regular bills' : 'Day-to-day spending'
  const spendingClass = (props.isRegular) ? 'spendingList-component regular' : 'spendingList-component'
  const action = (props.isRegular) ? 'bills' : 'payments'
  const buttonText = (toggleClass == 'hide') ? 'Show more' : 'Show less'
  const list = props.list.map((item,index) => {
    const itemDate = Moment(item.date, 'YYYY-MM-DD').format('D MMM YYYY')
    return (
      <li key={index}>
        <div className="date">{itemDate}</div>
        <div className="transaction">
          {item.merchantName}
          <span>&pound;{getCurrency(item.amount,0)}</span>
        </div>
      </li>
    )
  })

  return (
    <div className={spendingClass}>
      <h3>{title}<span>&pound;{getCurrency(props.total,0)}</span></h3>
      <ul className={toggleClass}>
        {list}
      </ul>
      <button className="btn-link" onClick={props.handleToggle} data-action={action}>{buttonText}</button>
    </div>
  )
}

export default SpendingList
