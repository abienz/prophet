import React from 'react'
import SVGTimeline from '../../assets/images/Icons/timeline-default@1x.svg'
import SVGActions from '../../assets/images/Icons/folders.svg'
import SVGAccounts from '../../assets/images/Icons/card.svg'
import SVGBudgets from '../../assets/images/Icons/image.svg'
import SVGSettings from '../../assets/images/Icons/gear.svg'

const Nav = (props) => {
  return(
    <nav className="nav-component">
      <ul>
        <li>
          <button onClick={props.handleBtn} data-action="timeline" className="btn-timeline">
            <svg width="22" height="22" xmlns="http://www.w3.org/2000/svg">
              <defs>
                <rect id="a" x="8" y="15.5" width="12" height="3" rx="1"/>
              </defs>
              <g fillRule="evenodd">
                <rect x="2.5" y="4" width="3" height="2" rx="1"/>
                <rect x="8.5" y="4" width="11" height="2" rx="1"/>
                <rect x="2.5" y="10" width="3" height="2" rx="1"/>
                <rect x="8.5" y="10" width="8" height="2" rx="1"/>
                <rect x="2.5" y="16" width="3" height="2" rx="1"/>
                <rect x="8.5" y="16" width="11" height="2" rx="1"/>
              </g>
            </svg>
            Timeline
          </button>
        </li>
        <li>
          <button onClick={props.handleBtn} data-action="actions" className="btn-actions">
            <svg width="22" height="22" xmlns="http://www.w3.org/2000/svg">
              <g fillRule="evenodd">
                <rect x="2.5" y="9.5" width="17" height="10" rx="2"/>
                <path d="M3.45 7.1h15.1A1.6 1.6 0 0 0 17 5.9H5a1.6 1.6 0 0 0-1.55 1.2zM4.45 3.6h13.1A1.6 1.6 0 0 0 16 2.4H6a1.6 1.6 0 0 0-1.55 1.2z" strokeWidth=".8"/>
              </g>
            </svg>
            Actions
          </button>
        </li>
        <li>
          <button onClick={props.handleBtn} data-action="accounts" className="btn-accounts">
            <svg width="22" height="22" xmlns="http://www.w3.org/2000/svg">
              <g fillRule="evenodd">
                <path d="M1.5 10v7.5A1.5 1.5 0 0 0 3 19h16a1.5 1.5 0 0 0 1.5-1.5V10h-19z"/>
                <path fill="#C9C6C6" fillRule="nonzero" d="M3 14.5h4v3H3z"/>
                <path d="M1.5 7h19v-.5A1.5 1.5 0 0 0 19 5H3a1.5 1.5 0 0 0-1.5 1.5V7z"/>
              </g>
            </svg>
            Accounts
          </button>
        </li>
        <li>
          <button onClick={props.handleBtn} data-action="budgets" className="btn-budgets">
            <svg width="22" height="22" xmlns="http://www.w3.org/2000/svg">
              <g strokeWidth=".8" fillRule="evenodd">
                <path d="M1.122 10.331c.549 1.182 2.885 2.07 5.683 2.07 2.799 0 5.134-.888 5.683-2.07.08.173.122.35.122.534v.564c0 1.437-2.599 2.603-5.805 2.603C3.599 14.032 1 12.866 1 11.429v-.564c0-.183.042-.361.122-.534zM1 8.43c.028-.617.069-.983.122-1.098.549 1.182 2.885 2.07 5.683 2.07 2.799 0 5.134-.888 5.683-2.07.053.115.094.481.122 1.098 0 1.437-2.599 2.603-5.805 2.603C3.599 11.032 1 9.866 1 8.429zm19.98.59v.564c0 1.437-3.36 2.603-6.566 2.603-.29 0-.431-1.548-.42-3.04.01-1.39.15-2.73.42-2.73 3.206 0 6.566 1.165 6.566 2.602zm-6.867 8.378c.038-.392.244-.588.618-.588 2.798 0 5.578-.888 6.127-2.07.08.173.122.35.122.534v.564c0 1.437-3.043 2.603-6.249 2.603-.641 0-.618-.727-.618-1.043zm-.071-3c.037-.392.243-.588.618-.588 2.798 0 5.65-.888 6.198-2.07.08.173.122.35.122.534v.564c0 1.437-3.114 2.603-6.32 2.603-.641 0-.618-.727-.618-1.043zm-12.92-1.003c.549 1.182 2.885 2.07 5.683 2.07 2.799 0 5.134-.888 5.683-2.07.08.172.122.35.122.533v.565c0 1.437-2.599 2.602-5.805 2.602C3.599 17.094 1 15.93 1 14.492v-.565c0-.183.042-.36.122-.533zm0 2.906c.549 1.182 2.885 2.07 5.683 2.07 2.799 0 5.134-.888 5.683-2.07.08.172.122.35.122.533v.564c0 1.438-2.599 2.603-5.805 2.603C3.599 20 1 18.835 1 17.397v-.564c0-.183.042-.361.122-.533zM1 4.61c0-1.437 2.6-2.602 5.805-2.602 3.206 0 5.805 1.165 5.805 2.602v.565c0 1.437-2.599 2.603-5.805 2.603C3.599 7.778 1 6.612 1 5.175V4.61z" id="a"/>
              </g>
             </svg>
            Budgets
          </button>
        </li>
        <li>
          <button onClick={props.handleBtn} data-action="settings" className="btn-settings">
            <svg width="22" height="22" xmlns="http://www.w3.org/2000/svg">
              <g fillRule="nonzero">
                <path d="M19.372 8.272l-.524-1.266c-1.55.372-2.69.241-3.396-.464-.702-.703-.831-1.841-.46-3.39l-1.265-.524c-.834 1.357-1.732 2.07-2.727 2.07-.995 0-1.892-.713-2.725-2.07l-1.268.525c.372 1.55.242 2.688-.462 3.39-.704.705-1.843.835-3.393.464l-.524 1.266c1.36.835 2.071 1.733 2.07 2.727 0 .995-.713 1.893-2.07 2.725l.522 1.266c1.551-.371 2.69-.24 3.392.464.704.704.835 1.843.465 3.393l1.268.524c.833-1.356 1.73-2.068 2.725-2.068.995 0 1.893.712 2.727 2.068l1.266-.524c-.372-1.55-.242-2.689.462-3.393.704-.704 1.843-.835 3.393-.463l.523-1.267c-1.358-.832-2.07-1.73-2.07-2.725 0-.994.712-1.892 2.07-2.728z"/>
                <path d="M11.001 14.503a3.5 3.5 0 0 0 3.499-3.501 3.5 3.5 0 1 0-7 0 3.501 3.501 0 0 0 3.501 3.501z"/>
              </g>
            </svg>
            Settings
          </button>
        </li>
      </ul>
    </nav>
  )
}

export default Nav
