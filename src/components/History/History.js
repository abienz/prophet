import React from 'react'
import getCurrency from '../../helpers/Currency'

const getMaxBarWidth = (items) => {
  const sortedList = items.reduce(function(prev,curr) {
    return (prev.income > curr.income) ? prev: curr
  })

  return sortedList.income
}

const getWidth = (money, maxMoney) => {
  let width = ((money/maxMoney) * 235)
  return Number.parseInt(width)
}

const History = (props) => {
  const maxBarWidth = getMaxBarWidth(props.history);
  const history = props.history.map(function(item,index){
    if(index > 11) return
    return(
      <li key={index}>
        <div className="period">
          {item.period}
        </div>
        <div className="bars">
          <div className="income">
            <b style={{width: + getWidth(item.income, maxBarWidth)}}></b><span>&pound;{getCurrency(item.income,0)}</span>
          </div>
          <div className="spending">
            <b style={{width: + getWidth(item.spending, maxBarWidth)}}></b><span>&pound;{getCurrency(item.spending,0)}</span>
          </div>
        </div>
      </li>
    )
  })
  return (
    <section style={props.style} className="history-component">
      <button className="btn-link" data-action="hide" onClick={props.toggleHistory}>
        Hide
        <svg width="16" height="10" xmlns="http://www.w3.org/2000/svg">
          <path d="M14.4 0L16 1.667 9.6 8.333 8 10 6.4 8.333 0 1.667 1.6 0 8 6.667z" fill="#1D7B8A" fillRule="evenodd"/>
        </svg>
      </button>

      <h2>Your history</h2>
      <div className="key">
        <span>&nbsp;</span>Money in
        <span>&nbsp;</span>Money out
      </div>
      <ul>
        {history}
      </ul>
    </section>
  )
}

export default History
