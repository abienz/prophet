import React from 'react'
import Balance from '../Balance/Balance'
import logo from '../../assets/images/nwLogo.png'

const AccountCard = (props) => {
  return(
    <div className="accountCard-component">
      <h2>{props.accountData.accountName}</h2>
      <p>
        {props.accountData.accountNumber} | {props.accountData.sortCode}
      </p>
      <Balance balance={props.accountData.balance} availableBalance={props.accountData.available}/>
      <img src={logo} alt="bank logo"/>
    </div>
  )
}

export default AccountCard

AccountCard.defaultProps = {
  accountData : {
    accountName: 'Reward account',
    accountNo: '12345678',
    sortCode: '01-60-09',
    balance: 3210.37,
    availableBalance: 3687.98
  }
}
