import React from 'react'

const Balance = (props) => {
  const balanceInteger = new Intl.NumberFormat('en-GB', {maximumFractionDigits: 0}).format(props.balance)
  const balanceFraction = (props.balance % 1).toFixed(2).substr(2)
  const availableBalance = new Intl.NumberFormat().format(props.availableBalance)
  const balanceOutput = (props.notShowDecimal) ? <span><b>&pound;{balanceInteger}</b></span> : <span><b>&pound;{balanceInteger}</b>.{balanceFraction}</span>
  const availableBalanceOutput = (props.notShowDecimal) ? <span className="available">Available &pound;{new Intl.NumberFormat('en-GB', {maximumFractionDigits: 0}).format(props.availableBalance)}</span> : <span className="available">Available &pound;{availableBalance}</span>
  return (
    <div className="balance-component">
      {balanceOutput}
      {availableBalanceOutput}
    </div>
  )
}

export default Balance

Balance.defaultProps = {
  balance: 3210.37,
  availableBalance: 3687.98
}
