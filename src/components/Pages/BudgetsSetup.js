import React from 'react'
import { Transition } from 'react-spring'
import Balance from '../Balance/Balance'
import History from '../History/History'
import getCurrency from '../../helpers/Currency'

export default class BudgetsSetup extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      viewHistory: 0,
      setBudget: Math.ceil(this.props.pageData.spentLastYear),
      budgetWarning: false
    }

    this.toggleHistory = this.toggleHistory.bind(this)
    this.increaseBudget = this.increaseBudget.bind(this)
    this.decreaseBudget = this.decreaseBudget.bind(this)
    this.stopTimer = this.stopTimer.bind(this)
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.setBudget > this.props.pageData.accountBalances[0] && !this.state.budgetWarning) {
      this.setState({
        budgetWarning: true
      })
    }
  }

  increaseBudget(e){
    this.timer = setInterval(
      () => this.setState({setBudget: this.state.setBudget+5}),
      100
    )
  }

  decreaseBudget(e){
    this.timer = setInterval(
      () => this.setState({setBudget: this.state.setBudget-5}),
      100
    )

    if(this.state.setBudget-5 <= this.props.pageData.accountBalances[0]) {
      this.setState({budgetWarning: false})
    }
  }

  stopTimer(e) {
    clearInterval(this.timer)
  }

  toggleHistory(e){
    this.props.handleClick(e)
    this.setState({viewHistory: !this.state.viewHistory})
  }

  render(){
    const toggle = this.state.viewHistory
    const setBudget = this.state.setBudget
    const budgetPerDay = setBudget/30
    const remainingBudget = setBudget - this.props.pageData.spent
    const budgetWarning = (this.state.budgetWarning) ? (
      <div className="budgetWarning">
        <svg width="16" height="16" xmlns="http://www.w3.org/2000/svg">
          <g fill="none" fillRule="evenodd">
            <path d="M8 0a8 8 0 1 1 0 16A8 8 0 0 1 8 0z" fill="#333"/>
            <path d="M7.265 10.918a1.032 1.032 0 0 1 1.47 0 1.062 1.062 0 0 1 0 1.488 1.03 1.03 0 0 1-1.47 0 1.06 1.06 0 0 1 0-1.488M8.948 8.55V3.925A.937.937 0 0 0 8 3a.937.937 0 0 0-.947.925v4.626c0 .511.424.925.947.925a.937.937 0 0 0 .948-.925" fill="#FBBA20"/>
          </g>
        </svg>
        Try reducing budget to stay in credit
      </div>
    ) : null

    return(
      <section className="page-component">
        {budgetWarning}
        <div className="budgetsHead">
          <h2>Today's balance</h2>
          <Balance balance={this.props.pageData.accountBalances[0]} availableBalance={this.props.pageData.availableBalances[0]} notShowDecimal="true"/>
        </div>

        <div className="breakdown">
          <ul>
            <li>Your estimated income is <b>&pound;{getCurrency(this.props.pageData.income,0)}</b></li>
            <li>You've spent <b>&pound;{getCurrency(this.props.pageData.spent,0,true)}</b> so far this month.</li>
            <li>Last December you spent <b>&pound;{getCurrency(this.props.pageData.spentLastYear,0,true)}</b></li>
          </ul>
          <button className="btn-link" data-action="viewHistory" onClick={this.toggleHistory}>View your history</button>
        </div>

        <div className="budgetCalculator">
          <p>
            How much do you want to budget for {this.props.currentMonth}?
          </p>

          <div>
            <button className="btn-mini" onMouseDown={this.decreaseBudget} onMouseUp={this.stopTimer}>-</button>
            <span>&pound;{getCurrency(setBudget, 0)}</span>
            <button className="btn-mini" onMouseDown={this.increaseBudget} onMouseUp={this.stopTimer}>+</button>
          </div>

          <ul>
            <li>
              Spent so far <b>&pound;{getCurrency(this.props.pageData.spent,0, true)}</b>
            </li>
            <li>
              Remaining budget <b>&pound;{getCurrency(remainingBudget,0)}</b>
            </li>
            <li>
              That's <b>&pound;{getCurrency(budgetPerDay,0)} <em>per day</em></b>
            </li>
          </ul>
        </div>

        <div className="mainButtonContainer">
          <button className="btn-setupBudget" data-action="setupBudget" onClick={this.props.handleClick}>Set up my budget</button>
        </div>

        <Transition items={toggle} from={{transform:'translateY(100%)'}} enter={{transform:'translateY(0)'}} leave={{transform:'translateY(100%)'}}>
          {toggle => toggle && (style => <History style={style} toggleHistory={this.toggleHistory} history={this.props.pageData.history}/>)}
        </Transition>
      </section>
    )
  }
}

BudgetsSetup.defaultProps = {
  currentMonth: 'December',
  income: 1500,
  lastPeriod: 600,
  perDay: 25,
  remaining: 500,
  spent: 100
}
