import React from 'react'
import SpendingList from '../SpendingList/SpendingList'
import getCurrency from '../../helpers/Currency'
import budgetsData from '../../assets/json/budgets+public+tracking.json'

export default class BudgetsView extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showBills: false,
      showPayments: false
    }

    this.handleToggle = this.handleToggle.bind(this)
  }

  handleToggle(e) {
    const action = e.target.dataset["action"]

    switch(action) {
      case 'bills' :
        this.setState({showBills: !this.state.showBills})
        break
      case 'payments' :
        this.setState({showPayments: !this.state.showPayments})
        break
    }
  }

  render() {
    const initialValue = 0
    const dayToDayBills = budgetsData.transactions.filter((item) => item.type == "day_to_day")
    const regularBills = budgetsData.transactions.filter((item) => item.type == "bill")
    const dayToDayBillsTotal = dayToDayBills.reduce((accumulator, current) => accumulator + current.amount, initialValue)
    const regularBillsTotal = regularBills.reduce((accumulator, current) => accumulator + current.amount, initialValue)
    const dayToDayWidth = dayToDayBillsTotal/budgetsData.budget * 100
    const regularWidth = regularBillsTotal/budgetsData.budget * 100
    const showBills = (this.state.showBills) ? 'show' : 'hide'
    const showPayments = (this.state.showPayments) ? 'show' : 'hide'

    return(
      <section className="page-component">
        <div className="budgetsView">
          <button className="btn-prev" data-action="prevMonth">{budgetsData.previousPeriodLabel}</button>
          <h2>{budgetsData.periodLabel}</h2>
          <div className="budget">
            &pound;{budgetsData.budget}
            <button className="btn-link" data-action="editBudget" onClick={this.props.handleClick}>Edit</button>
          </div>
        </div>

        <div className="budgetOverview">
          <ul>
            <li>
              Spent so far <b>&pound;{getCurrency(budgetsData.spent,0)}</b>
              <em>(<b>&pound;{getCurrency(budgetsData.spentPerDay,0)}</b> per day)</em>
            </li>
            <li>
              Remaining budget <b>&pound;{getCurrency(budgetsData.remainingBudget,0)}</b>
              <em>(<b>&pound;{getCurrency(budgetsData.remainingPerDay,0)}</b> per day)</em>
            </li>
          </ul>
        </div>

        <h3>{budgetsData.header}</h3>
        <p>{budgetsData.body}</p>

        <div className="budgetBars">
          <div className="bars">
            <span className="regularBills" style={{"width" : regularWidth  + "%"}}>&pound;{getCurrency(regularBillsTotal,0)}</span>
            <span className="dayToDay" style={{"width" : dayToDayWidth + "%"}}>&pound;{getCurrency(dayToDayBillsTotal,0)}</span>
          </div>
          <p>&pound;{getCurrency(budgetsData.spent,0)} of &pound;{getCurrency(budgetsData.budget,0)}</p>
        </div>

        <div className="spending">
          <SpendingList total={regularBillsTotal} list={regularBills} isRegular={true} showAll={showBills} handleToggle={this.handleToggle}/>
          <SpendingList total={dayToDayBillsTotal} list={dayToDayBills} showAll={showPayments} handleToggle={this.handleToggle}/>

          <p>
            Some of your spending may take a few days to come out of your account. So your balance may be showing higher than it really is.
          </p>
        </div>
      </section>
    )
  }
}
