import React from 'react'
import AccountCard from '../AccountCard/AccountCard'
import accountData from '../../assets/json/ob-accounts+public+accounts+overall.json'

const Accounts = (props) => {
  const accounts = accountData.accounts.map((item, index) => {
    return <AccountCard key={index} accountData={item}/>
  })
  return(
    <section className="pages-component">
      {accounts}
    </section>
  )
}

export default Accounts
