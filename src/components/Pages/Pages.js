import React from 'react'
import Timeline from './Timeline'
import Actions from './Actions'
import Accounts from './Accounts'
import Budgets from './Budgets'
import Settings from './Settings'

const PAGE = {
  timeline: Timeline,
  actions: Actions,
  budgets: Budgets,
  accounts: Accounts,
  settings: Settings
}

const Pages = (props) => {
  const Handler = PAGE[props.page] || Timeline

  return(
    <Handler {...props}/>
  )
}

export default Pages
