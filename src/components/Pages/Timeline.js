import React from 'react'
import TimelineCard from '../TimelineCard/TimelineCard'

const Timeline = (props) => {
  return(
    <section className="timeline-componenet">
      <TimelineCard type="roundup" />
    </section>
  )
}

export default Timeline
