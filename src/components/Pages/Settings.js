import React from 'react'

const Settings = (props) => {
  return(
    <section className="page-component settings">
      <h2>Mimo</h2>
      <ul>
        <li><button>Accounts</button></li>
        <li><button>Budgeting</button></li>
      </ul>
      <h2>Security</h2>
      <ul>
        <li>Touch ID</li>
      </ul>
      <p>Any fingerprint registered under Touch ID in your device settings, can be used to log into the app.</p>
    </section>
  )
}

export default Settings
