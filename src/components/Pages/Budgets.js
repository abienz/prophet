import React from 'react'
import BudgetsView from './BudgetsView'
import BudgetsSetup from './BudgetsSetup'

const PAGE = {
  budgetsView: BudgetsView,
  budgetsSetup: BudgetsSetup
}

const Budgets = (props) => {
  let page = (props.hasBudgets) ? 'budgetsView' : 'budgetsSetup'
  const Handler = PAGE[page] || BudgetsSetup
  
  return(
    <Handler {...props}/>
  )
}

export default Budgets
