import React from 'react'
import { Transition } from 'react-spring'
import Header from '../Header/Header'
import Pages from '../Pages/Pages'
import Nav from '../Nav/Nav'
import budgetsData from '../../assets/json/budgets+public+setup.json'

class App extends React.Component {
  constructor() {
    super()
    this.state = {
      page: 'timeline',
      pageData: {},
      heading: 'Today',
      overlay: false,
      hasBudget: false
    }

    this.handleNav = this.handleNav.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }

  handleNav(e) {
    const heading = (e.target.dataset['action'] == 'timeline') ? 'Today' : e.target.dataset['action']
    const pageData = (e.target.dataset['action'] == 'budgets') ? budgetsData : {}
    this.setState({
      page: e.target.dataset['action'],
      pageData: pageData,
      heading: heading
    })
  }

  handleClick(e) {
    const action = e.target.dataset['action']

    switch(action) {
      case 'viewHistory' :
        this.setState({overlay: !this.state.overlay})
        break
      case 'hide' :
        this.setState({overlay: !this.state.overlay})
        break
      case 'setupBudget' :
        this.setState({hasBudget: !this.state.hasBudget})
        break
      case 'editBudget' :
        this.setState({hasBudget: !this.state.hasBudget})
        break
      default:
        break
    }
  }

  render() {
    const className = 'app-component ' + this.state.page
    const overlay = this.state.overlay
    return(
      <div className={className}>
        <Header heading={this.state.heading}/>
        <Pages 
          pageData={this.state.pageData} 
          page={this.state.page} 
          handleClick={this.handleClick}
          hasBudgets={this.state.hasBudget}/>
        <Nav handleBtn={this.handleNav}/>
        <Transition items={overlay} from={{opacity:0}} enter={{opacity:1}} leave={{opacity:0}}>
          {overlay => overlay && (style => <div style={style} className="bgOverlay"></div>)}
        </Transition>
      </div>
    )
  }
}

export default App;
